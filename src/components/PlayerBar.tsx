import React from 'react';
import {
    Text,
    Image,
    View,
    StyleProp,
    ViewStyle,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';
import { colors } from '../Constants';

type PlayerBarProps = {
    playerName: string;
    containerStyle?: StyleProp<ViewStyle>;
};

const PlayerBar = (props: PlayerBarProps) => {
    return (
        <View style={props.containerStyle}>
        <LinearGradient colors={colors.primaryGradient} style={styles.playerBarContainer}>
            <Image 
                style={styles.playerBarFlag} 
                source={require('../../assets/images/flag.png')}
            />
            <Text style={styles.playerBarName}>{props.playerName}</Text>
        </LinearGradient>
        </View>
    );
};

export default PlayerBar;
  