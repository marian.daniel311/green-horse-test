import React, { useState, useEffect, useRef } from 'react';
import {
    Text,
    Image,
    View,
    Animated,
    TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation, useRoute } from '@react-navigation/native';
import Modal from 'react-native-modal';
import { colors } from '../Constants';
import styles from './styles';

type HeaderProps = {
    level: number;
    gold: number;
};

const Header = (props: HeaderProps) => {
    const [isMenuVisible, setIsMenuVisible ] = useState(false);
    const fadeAnim = useRef(new Animated.Value(0)).current;
    const updateAnim = useRef(new Animated.Value(0)).current;
    const navigation = useNavigation();
    const route = useRoute();

    const fadeIn = () => {
        fadeAnim.stopAnimation();
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 500,
          delay: 500,
          useNativeDriver:true
        }).start();
    };

    const fadeOut = () => {
        fadeAnim.stopAnimation();
        Animated.timing(fadeAnim, {
            toValue: 0,
            duration: 100,
            useNativeDriver:true
        }).start();
    };

    let closeMenu = () => {
        setIsMenuVisible(false);
    };

    let navigate = (screenName:string) => {
        if(screenName !== route.name){
            closeMenu();
            navigation.navigate(screenName);
        }
    };

    const updateAnimStart = () => {
        updateAnim.stopAnimation();
        Animated.timing(updateAnim, {
          toValue: 1,
          duration: 600,
          useNativeDriver:true
        }).start(() => {
            updateAnim.setValue(0);
        });
    };

    useEffect(() => {
        if(route.name === 'Challenge')
        updateAnimStart();
    },[props.gold]);

    useEffect(() => {
        if(isMenuVisible){
            fadeOut();
        }
        else{
            fadeIn();
        }
    },[isMenuVisible]);

    const scaleGold = updateAnim.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 1.2]
    });
    const opacity = updateAnim.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 0.4]
    });

    return (
        <>
        <LinearGradient colors={colors.primaryGradient} style={styles.headerContainer}>
            <View>
                <TouchableOpacity 
                    hitSlop={{right:10,bottom:10,left:10,top:10}}
                    onPress={_ => setIsMenuVisible(!isMenuVisible)}
                >
                    <View style={styles.burgerMenuLine}></View>
                    <View style={styles.burgerMenuLineMiddle}></View>
                    <View style={styles.burgerMenuLine}></View>
                    <Animated.Image 
                        source={require('../../assets/images/alert.png')}
                        style={[styles.burgerMenuAlert,{
                            opacity: fadeAnim
                        }]}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.headerRightContainer}>
                <View style={styles.headerLevelContainer}>
                    <Image 
                        source={require('../../assets/images/shirt.png')}
                        style={styles.headerLevelIcon}
                    />
                    <Text style={styles.headerLevel}>{props.level}</Text>
                </View>
                <View style={styles.headerUnitsContainer}>
                    <Image 
                        source={require('../../assets/images/units.png')}
                        style={styles.headerUnitsIcon}
                    />
                    <Animated.Text style={[styles.headerUnits,
                         {transform: [{ scaleY: scaleGold }, {scaleX:scaleGold}], opacity: opacity}
                    ]}>{props.gold}</Animated.Text>
                </View>
            </View>
        </LinearGradient>

        <Modal 
            isVisible={isMenuVisible} 
            onBackdropPress={closeMenu} 
            useNativeDriver={true} 
            useNativeDriverForBackdrop={true}
            backdropOpacity={0}
            animationIn="slideInLeft"
            animationOut="fadeOut"
            animationInTiming={300}
            animationOutTiming={10}
        >
            <View style={styles.menu}>
                <TouchableOpacity 
                    style={styles.menuItem}
                    onPress={_ => navigate("Profile")}
                >
                    <Image 
                        source={require('../../assets/images/profile.png')}
                        style={styles.menuItemIcon}
                    />
                    <Text style={styles.menuItemText}>Profile</Text>
                    {route.name === 'Challenge' ?
                    <Image 
                        source={require('../../assets/images/alert.png')}
                        style={styles.menuItemAlert}
                    />
                    : null}
                </TouchableOpacity>
                <View style={styles.menuItemDelimitator}></View>
                <TouchableOpacity 
                    style={styles.menuItem}
                    onPress={_ => navigate("Challenge")}
                >
                    <Image 
                        source={require('../../assets/images/skill.png')}
                        style={styles.menuItemIcon}
                    />
                    <Text style={styles.menuItemText}>Skill Challenge</Text>
                    {route.name === 'Profile' ?
                    <Image 
                        source={require('../../assets/images/alert.png')}
                        style={styles.menuItemAlert}
                    />
                    : null}
                </TouchableOpacity>
            </View>
        </Modal>
        </>
    );
};

export default Header;
  