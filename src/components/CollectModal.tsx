import React, { useContext, useRef } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    Alert,
    Animated,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import styles from './styles';
import { colors } from '../Constants';
import ProfileContext from '../context/context';
import api from '../api';

type CollectModalProps = {
    isVisible: boolean,
    onDismiss: () => void,
    rewardValue: number,
};

const CollectModal = (props: CollectModalProps) => {
    const { profileData, onCollectReward } = useContext(ProfileContext);
    const rotateAnim = useRef(new Animated.Value(0)).current;

    let collectReward = async (gold: number) => {
       rotateCoin();
       let data = await api.collectReward(profileData.userId);
       if(!data.success){
        Alert.alert(
            "Something went wrong",
            "Please try again later!",
            [
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ],
            { cancelable: true }
          );
          return;
       }
        onCollectReward(gold);
        props.onDismiss();
    };

    const rotateCoin = () => {
        rotateAnim.setValue(0);
        Animated.timing(rotateAnim, {
          toValue: 1,
          duration: 800,
          useNativeDriver:true
        }).start();
    };

    const rotateValue = rotateAnim.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
    });

    return (
    <Modal isVisible={props.isVisible} onBackButtonPress={props.onDismiss} onBackdropPress={props.onDismiss} useNativeDriver={true} useNativeDriverForBackdrop={true}>
    <View style={styles.modalContainer}>
        <LinearGradient colors={colors.borderGradient} style={styles.modalInnerContainer}>
        <View style={styles.modalContent}>
            <LinearGradient colors={colors.primaryGradient} style={styles.modalHeaderContainer}>
                <Text style={styles.modalHeaderTitle}>GIFT</Text>
            </LinearGradient>
            <View style={styles.modalMiddleContainer}>
                <View style={styles.modalImageContainer}>
                    <Image 
                        source={require('../../assets/images/sparkle.png')}
                        style={styles.modalSparkleImage}
                    />
                    <Image 
                        source={require('../../assets/images/open_gift.png')}
                        style={styles.modalGiftImage}
                    />
                </View>
                <Text style={styles.modalRewardLabel}>Rewards:</Text>
                <View style={styles.modalRewardContainer}>
                    <Text style={styles.modalRewardValue}>+{props.rewardValue}</Text>
                    <Animated.Image 
                        source={require('../../assets/images/units.png')}
                        style={[styles.modalRewardIcon,
                            {transform: [{ rotateY: rotateValue }]}
                        ]}
                    />
                </View>
            </View>
            <View style={styles.modalBottomContainer}>
                <TouchableOpacity style={styles.modalCollectButton} onPress={_ => collectReward(props.rewardValue)}>
                    <Text style={styles.modalCollectButtonText}>COLLECT</Text>
                </TouchableOpacity>
            </View>
        </View>
        </LinearGradient>
    </View>
    </Modal>
    );
};

export default CollectModal;
  