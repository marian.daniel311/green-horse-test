import React, { useEffect, useState } from 'react';
import {
    Text,
    Image,
    View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';
import { colors } from '../Constants';

enum medalTypes {
    skill = 'skill',
    player = 'player',
};

type AchievementProps = {
    type: string;
    amount: string;
};

const Achievement = (props: AchievementProps) => {
    const [medalType,setMedalType] = useState(medalTypes.skill);

    useEffect(() => {
        if(props.type === medalTypes.skill) setMedalType(medalTypes.skill);
        else setMedalType(medalTypes.player);
    },[]);

    let getName = () => {
        switch (medalType) {
            case  medalTypes.skill:
                return "Skill Master";
            default:
                return "Player Challenge";
        };
    };

    let getIcon = () => {
        switch (medalType) {
            case  medalTypes.skill:
                return require('../../assets/images/medal1.png');
            default:
                return require('../../assets/images/medal2.png');
        };
    };
    
    return (
        <View style={styles.achievementContainer}>
            <LinearGradient colors={colors.primaryGradient} style={styles.achievementGradient}>
                <Text style={styles.achievementName}>{getName()}</Text>
            </LinearGradient>
            <View style={styles.achievementBottomContainer}>
                <View style={styles.achievementImageContainer}>
                    <Image 
                        source={getIcon()}
                        style={styles.medalImage}
                    />
                    <View style={styles.achievementQuantityContainer}>
                        <Text style={styles.achievementQuantity}>{props.amount}</Text>
                    </View>
                </View>
            </View>
        </View>
    );
};

export default Achievement;
  