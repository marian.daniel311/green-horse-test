import React from 'react';
import {
    Text,
    View,
    Image,
} from 'react-native';
import styles from './styles';

type StatProps = {
    userId: number;
    userName: string;
    skill: number;
    active?: boolean;
    last?: boolean;
    position: number;
};

const ChallengeStat = (props:StatProps) => {
    let getIcon = () => {
        if(props.position == 1){
            return(
                <Image 
                    source={require('../../assets/images/gold_gift.png')}
                    style={{width:37,height:37,resizeMode:'contain'}}
                />
            )
        }
        if(props.position == 2){
            return(
                <Image 
                    source={require('../../assets/images/silver_gift.png')}
                    style={{width:34,height:34,resizeMode:'contain'}}
                />
            )
        }
        if(props.position == 3){
            return(
                <Image 
                    source={require('../../assets/images/bronze_gift.png')}
                    style={{width:30,height:30,resizeMode:'contain'}}
                />
            )
        }
        return null;
    };

    let getValue = (value: number) => {
        if(value >= 1000){
            let decimal = Math.floor((value % 1000)/100);
            if(decimal > 0) return `${String(Math.floor(value/1000))}.${String(decimal)}K`;
            return `${String(Math.floor(value/1000))}K`;
        }
        return String(value);
    };

    return (
        <View style={[
            styles.statContainer,
            props.active && styles.statActiveContainer,
            props.last && styles.statLastContainer,
            props.position == 1 && styles.statFirstContainer
        ]}>
            <View style={styles.statLeftContainer}>
                <Text style={styles.statPosition}>{props.position}</Text>
                <Image 
                    source={require('../../assets/images/default_avatar.png')}
                    style={styles.statPlayeraVatar}
                />
                <Text style={[styles.statUsername, props.active && {fontWeight:'bold'}]}>{props.userName}</Text>
            </View>
            <View style={styles.statRightContainer}>
                <View style={styles.statValueContainer}>
                    <Image 
                        source={require('../../assets/images/training.png')}
                        style={styles.statValueIcon}
                    />
                    <Text style={styles.statValue}>{getValue(props.skill)}</Text>
                </View>
                <View style={styles.statGiftIcon}>
                    {getIcon()}
                </View>
            </View>
        </View>
    );
};

export default ChallengeStat;
  