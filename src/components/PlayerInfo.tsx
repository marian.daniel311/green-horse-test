import React from 'react';
import {
    Text,
    Image,
    View,
    StyleProp,
    ViewStyle,
} from 'react-native';
import styles from './styles';

type PlayerInfoProps = {
    level: number;
    teamName: string;
    containerStyle?: StyleProp<ViewStyle>;
};

const PlayerInfo = (props: PlayerInfoProps) => {
    return (
        <View style={props.containerStyle}>
            <View style={styles.playerInfoContainer}>
                <Image 
                    source={require('../../assets/images/default_avatar.png')}
                    style={styles.playerInfoAvatar}
                />
                <View style={styles.playerInfoRightContainer}>
                    <View style={styles.playerInfoLine}>
                        <Image 
                            source={require('../../assets/images/shirt.png')}
                            style={styles.playerInfoLineIcon}
                        />
                        <Text style={styles.playerInfoLineValue}>{props.level}</Text>
                    </View>
                    <View style={styles.playerInfoLine}>
                        <Image 
                            source={require('../../assets/images/team.png')}
                            style={styles.playerInfoTeamIcon}
                        />
                        <Text style={styles.playerInfoLineTeamName}>{props.teamName}</Text>
                    </View>
                </View>
            </View>
        </View>
    );
};

export default PlayerInfo;
  