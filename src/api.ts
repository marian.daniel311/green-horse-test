const BASE_URL:string = "https://greenhorsegames.com/tests/android";

const getProfileData = async () => {
  const data = await fetch(`${BASE_URL}/profile.php`)
    .then(response => response.json())
    .then(data => data);
   return data;
};

const getChallenge = async () => {
  const data = await fetch(`${BASE_URL}/challenge.php`)
    .then(response => response.json())
    .then(data => data);
   return data;
};

const collectReward = async (userId: number) => {
  const data = await fetch(`${BASE_URL}/collect.php?userId=${userId}`)
    .then(response => response.json())
    .then(data => data);
   return data;
};


const api = {
    getProfileData,
    getChallenge,
    collectReward,
}

export default api;