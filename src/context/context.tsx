import React from 'react';
import { profileType } from '../../App';

export type ProfileContextProps = {
  profileData: profileType;
  onCollectReward: (gold: number) => void;
};

export default React.createContext<ProfileContextProps>({
  profileData : { 
    userId: 1,
    level: 1,
    gold: 0,
    userName: "",
    clubName: "",
    medals: []
  },
  onCollectReward: (gold: number) => {},
});

