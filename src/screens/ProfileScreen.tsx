import React , { useContext } from 'react';
import {
    View,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import  { PlayerInfo, Achievement, PlayerBar } from '../components';
import { Screen } from '../routers/MainNavigator';
import ProfileContext from '../context/context';
import styles from './styles';

const Profile = () => {
    const { profileData }= useContext(ProfileContext);

    return (
        <View style={{flex:1}}>
        <ScrollView style={{flex:1}}>
            <PlayerBar playerName={"PLAYER NAME"} containerStyle={styles.playerBarContainerStyle} />
            <PlayerInfo level={profileData.level} teamName={profileData.clubName} containerStyle={styles.playerInfoContainer} />
            <View style={styles.playerMedalsContainer}>
                {profileData.medals.map((medal, index) => {
                    return <Achievement 
                        key={index} 
                        type={medal.type}
                        amount={medal.amount}
                    />
                })}       
            </View>
           
        </ScrollView>
        </View>
        
    );
};

const ProfileScreen = () => {
    return(
        <Screen>
            <Profile />
        </Screen>
    )
};
export default ProfileScreen;
  