import React, { useState, useEffect, useContext } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    ScrollView,
    Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import  { ChallengeStat, CollectModal } from '../components';
import { Screen } from '../routers/MainNavigator';
import styles from './styles';
import { colors } from '../Constants';
import ProfileContext from '../context/context';
import api from '../api';

export type statType = {
    userId: number;
    userName: string;
    skill: number;
};

type rewardType = {
    type: string;
    reward: number;
};

type challenge = {
    challengeGroup: statType[];
    uncollectedReward: rewardType;
}

const Challenge = () => {
    const [challenge,setChallenge] = useState<challenge | undefined>(undefined);
    const { profileData }= useContext(ProfileContext);
    const [isModalVisible,setIsModalVisible] = useState(false);

    useEffect(() => {
        async function getChallenge() {
            const data = await  api.getChallenge();
            setChallenge(data);
        }
        getChallenge();
    },[]);

    let showModal = () => {
        setIsModalVisible(true);
    };
    let hideModal = () => {
        setIsModalVisible(false);
    };

    return (
        <>
        {challenge ?
        <>
        <CollectModal isVisible={isModalVisible} onDismiss={hideModal} rewardValue={challenge.uncollectedReward.reward} />
        <ScrollView  style={{flex:1}}>
            <View style={styles.rankTableContainer}>
                <LinearGradient colors={colors.blueGradient} style={styles.rankTableGradient}>
                    <Image 
                        style={styles.rankTableHeaderIcon} 
                        source={require('../../assets/images/training.png')}
                    />
                    <Text style={styles.rankTableName}>Skill Challenge</Text>
                </LinearGradient>
                <View style={styles.rankTableBottomContainer}>
                    <Image 
                        style={styles.rankTableBackround} 
                        source={require('../../assets/images/stats_background.png')}
                    />
                    <View style={styles.rankTableCollectContainer}>
                        <Text style={styles.collectTitle}>Challenge Ended</Text>
                        <View style={styles.collectButtonContainer}>
                            <Image 
                                style={styles.collectIcon} 
                                source={require('../../assets/images/gold_gift.png')}
                            />
                            <TouchableOpacity onPress={_ => showModal()} hitSlop={{top:5,left:5,right:5,bottom:5}}>
                            <LinearGradient colors={colors.greenGradient} style={styles.collectButton}>
                                <Text style={styles.collectButtonText}>COLLECT</Text>
                            </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.statsContainer}>
                        {challenge.challengeGroup.map((stat, index) => {
                            return <ChallengeStat 
                                key={index}
                                position={index + 1}
                                userId={stat.userId} 
                                userName={stat.userName}
                                skill={stat.skill}
                                active={stat.userId == profileData.userId}
                                last={index == challenge.challengeGroup.length - 1}
                            />
                        })}           
                    </View>
                </View>
            </View>
        </ScrollView>
        </>
        :
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text style={{fontSize:18,color:'#fff'}}>Loading your challenge...</Text>
        </View>
        }
        </>
    );
};

const ChallengeScreen = () => {
    return(
        <Screen>
            <Challenge />
        </Screen>
    )
};
export default ChallengeScreen;
  