import React, { useContext } from 'react';
import { SafeAreaView, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator , TransitionPresets } from '@react-navigation/stack';

import ProfileScreen from '../screens/ProfileScreen';
import ChallengeScreen from '../screens/ChallengeScreen';
import { Header } from '../components';
import ProfileContext from '../context/context';

const MainStack = createStackNavigator();

type ScreenProps = {
  children: React.ReactNode;
};

export const Screen = (props: ScreenProps) => {
  return(
    <SafeAreaView style={{flex:1}}>
        <Image 
          source={require('../../assets/images/background.png')}
          style={{width:'100%',height:'100%',resizeMode:'cover',position:'absolute',top:0,left:0,}} 
        />
      {props.children}  
    </SafeAreaView>
  )
};


function MainNavigator() {
  const { profileData } = useContext(ProfileContext);
  return (
    <NavigationContainer>
      <MainStack.Navigator
       headerMode={'float'}
       screenOptions={{
          header: () =>  <Header level={profileData.level} gold={profileData.gold} />
        }}
      >
        <MainStack.Screen name="Profile" component={ProfileScreen} />
        <MainStack.Screen name="Challenge" component={ChallengeScreen}
           options={{
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
      </MainStack.Navigator>
    </NavigationContainer>
  );
}

export default MainNavigator;