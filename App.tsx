import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StatusBar,
  View,
  Text,
} from 'react-native';
import 'react-native-gesture-handler';
import MainNavigator from './src/routers/MainNavigator';
import { colors } from './src/Constants';
import ProfileContext from './src/context/context';
import { Screen } from './src/routers/MainNavigator';
import api from './src/api';

type medalsType = {
  type: string,
  amount: string,
};

export type profileType = {
  userId: number,
  level: number,
  gold: number,
  userName: string,
  clubName: string,
  medals: medalsType[],
};

function App() {
  const [profileData,setProfileData] = useState<profileType | undefined>(undefined);

  useEffect(() => {
      async function getProfile() {
          const data = await  api.getProfileData();
          setProfileData(data);
      }
      getProfile();
  },[]);

  const onCollectReward =  (gold: number) => {
    if(profileData){
      setProfileData({
        userId: profileData.userId,
        level: profileData.level,
        gold: profileData.gold + gold,
        userName: profileData.userName,
        clubName: profileData.clubName,
        medals: profileData.medals,
      });
    }
  };

  return (
    <SafeAreaView style={{flex:1}}>
      <StatusBar backgroundColor={colors.greyDark1} />
      {profileData ?
      <ProfileContext.Provider 
          value={{
            profileData: profileData,
            onCollectReward
          }}
      >
         <MainNavigator />
      </ProfileContext.Provider>  
      : 
      <Screen>
      <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
          <Text style={{color:'#fff',fontSize:20,}}>Loading your data...</Text>
      </View>
      </Screen>
      } 
    </SafeAreaView>
  );
}

export default App;